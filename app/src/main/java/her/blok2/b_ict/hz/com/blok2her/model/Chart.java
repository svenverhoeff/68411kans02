package her.blok2.b_ict.hz.com.blok2her.model;

import java.util.ArrayList;

/**
 * Created by anton on 21-1-16.
 * defines a chart
 */
public class Chart {
    public static ArrayList<MusicItem> musicItems = new ArrayList<MusicItem>();

    private ArrayList<String> description;


    public ArrayList<String> getDescription() {
        return description;
    }

    public void setDescription(ArrayList<String> description) {
        this.description = description;
        description.add("Album top 50");
    }

}
