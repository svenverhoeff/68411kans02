package her.blok2.b_ict.hz.com.blok2her.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import her.blok2.b_ict.hz.com.blok2her.R;
import her.blok2.b_ict.hz.com.blok2her.api.CallAPI;
import her.blok2.b_ict.hz.com.blok2her.api.OnTaskCompleted;
import her.blok2.b_ict.hz.com.blok2her.model.Album;
import her.blok2.b_ict.hz.com.blok2her.model.AlbumChart;
import her.blok2.b_ict.hz.com.blok2her.model.AlbumChartPosition;
import her.blok2.b_ict.hz.com.blok2her.model.Artist;
import her.blok2.b_ict.hz.com.blok2her.model.Band;
import her.blok2.b_ict.hz.com.blok2her.model.Chart;
import her.blok2.b_ict.hz.com.blok2her.model.ChartPosition;
import her.blok2.b_ict.hz.com.blok2her.model.Performer;
import her.blok2.b_ict.hz.com.blok2her.model.Song;
import her.blok2.b_ict.hz.com.blok2her.model.SongChart;
import her.blok2.b_ict.hz.com.blok2her.model.SongChartPosition;

public class MusicMainActivity extends AppCompatActivity  implements OnTaskCompleted {

    public static final String CARD_API = "http://46.105.120.168/cardapi/";
    private String author;
    private String programName;
    private final OnTaskCompleted onTaskCompleted = this;//enables call-back methods


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Button dataButton = (Button) findViewById(R.id.data_button);
        dataButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start DataActivity.class
                Intent myIntent = new Intent(MusicMainActivity.this,
                        DataActivity.class);
                startActivity(myIntent);
            }
        });
        Button artistsButton = (Button) findViewById(R.id.artists_button);
        artistsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start DataActivity.class
                Intent myIntent = new Intent(MusicMainActivity.this,
                        ArtistsActivity.class);
                startActivity(myIntent);
            }
        });
        Button albumButton = (Button) findViewById(R.id.albums_button);
        albumButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start DataActivity.class
                Intent myIntent = new Intent(MusicMainActivity.this,
                        AlbumsActivity.class);
                startActivity(myIntent);
            }
        });
        Button songsButton = (Button) findViewById(R.id.songs_button);
        songsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                // Start DataActivity.class
                Intent myIntent = new Intent(MusicMainActivity.this,
                        SongsActivity.class);
                startActivity(myIntent);
            }
        });
        exerciseMethod();
    }

    /**
     * method to do exercises for exam
     */
    private void exerciseMethod(){

        String urlString = CARD_API +String.format("/games/ip/numberofmoves");

        new CallAPI(onTaskCompleted, getString(R.string.listGames)).execute(urlString, getString(R.string.GET));


        //test-array of albums
        Band beatles=new Band("Beatles");
        Artist bowie=new Artist("David Bowie");
        Album stationToStation=new Album(bowie, "Station To Station");
        Album abbeyRoad=new Album(beatles, "Abbey Road (1969)");
        ArrayList<Album> albums = new ArrayList<>(Arrays.asList(
                abbeyRoad,stationToStation,
                new Album(beatles, "Please Please Me (1963)"),
                new Album(beatles, "With The Beatles (1963)"),
                new Album(beatles, "A Hard Day's Night (1964)"),
                new Album(beatles, "Beatles For Sale (1964)"),
                new Album(beatles, "Help (1965)"),
                new Album(beatles, "Rubber Soul (1965)"),
                new Album(beatles, "Revolver (1966)"),
                new Album(beatles, "Sgt. Pepper's Lonely Hearts Club Band (1967)"),
                new Album(beatles, "Magical Mystery Tour (1967)"),
                new Album(beatles, "1The Beatles (1968)"),
                new Album(beatles, "Yellow Submarine (1969)"),
                new Album(beatles, "Let It Be"),
                new Album(bowie, "Hunky Dory"),
                new Album(bowie, "The Rise And Fall Of Ziggy Stardust And The Spiders From Mars"),
                new Album(bowie, "Aladdin Sane"),
                new Album(bowie, "Pin Ups"),
                new Album(bowie, "Diamond Dogs"),
                new Album(bowie, "Young Americans"),
                new Album(bowie, "Heroes"),
                new Album(bowie, "Low"),
                new Album(bowie, "Stage"),
                new Album(bowie, "Lodger"),
                new Album(bowie, "Scary Monsters"),
                new Album(bowie, "Let's Dance"),
                new Album(bowie, "Tonight")
            )
        );

        //test-array of artists
        ArrayList<Performer> artists = new ArrayList<>(Arrays.asList(
                beatles,bowie,
                new Band("Bad Company"),
                new Band("Band"),
                new Band("Barclay James Harvest"),
                new Artist("Peter Bardens"),
                new Band("Be-Bop Deluxe"),
                new Band("Beach Boys"),
                new Band("Beethoven"),
                new Artist("Bill Bruford"),
                new Band("Bintangs"),
                new Artist("Bjork"),
                new Band("Black Keys"),
                new Band("Black Uhuru"),
                new Band("Blind Faith"),
                new Band("Blondie"),
                new Band("Blood, Sweat&Tears"),
                new Band("Booker T and the MG's"),
                new Band("Bootsy's Rubber Band"),
                new Band("Brainbox"),
                new Band("Bread"),
                new Band("Buena Vista Social Club"),
                new Band("Byrds")
            )
        );

        Song comeTogether=new Song("Come Together",abbeyRoad);
        Song something=new Song("Something",abbeyRoad);
        ArrayList<Song>songs =new ArrayList<Song>(Arrays.asList(
                comeTogether ,
                something,
                new Song("Maxwell's Silver Hammer",abbeyRoad),
                new Song("Oh! Darling",abbeyRoad),
                new Song("Octopus's Garden",abbeyRoad),
                new Song("I Want You (She's So Heavy)",abbeyRoad),
                new Song("Here Comes the Sun",abbeyRoad),
                new Song("Because",abbeyRoad),
                new Song("You Never Give Me Your Money",abbeyRoad),
                new Song("Sun King",abbeyRoad),
                new Song("Mean Mr. Mustard",abbeyRoad),
                new Song("Polythene Pam",abbeyRoad),
                new Song("She Came in Through the Bathroom Window",abbeyRoad),
                new Song("Golden Slumbers",abbeyRoad),
                new Song("Carry That Weight",abbeyRoad),
                new Song("The End",abbeyRoad),
                new Song("Her Majesty",abbeyRoad),
                new Song("Station to Station",stationToStation),
                new Song("Golden Years",stationToStation),
                new Song("Word On A Wing",stationToStation),
                new Song("TVC 15",stationToStation),
                new Song("Stay",stationToStation),
                new Song("Wild Is The Wind",stationToStation)
            )
        );

        //initialise variables to use
        author="@string/author";
        programName="Muziek";

        Album revolver=new Album();
        AlbumChart albumtop50=new AlbumChart();
        AlbumChartPosition Revolver=new AlbumChartPosition();




        revolver.setTitle("Revolver");
        Revolver.setPosition(3);


        //setup billboard hot 1 100 chart
        SongChart billboardChart=new SongChart();
        SongChartPosition songNumber1=new SongChartPosition();
        songNumber1.setPosition(1);
        songNumber1.setMusicItem(comeTogether);
        songNumber1.setChart(billboardChart);

        //setup album top 50 chart
        AlbumChart albumTop50 = new AlbumChart();
        AlbumChartPosition albumNumber2=new AlbumChartPosition();
        albumNumber2.setPosition(2);
        albumNumber2.setChart(albumTop50);
        albumNumber2.setMusicItem(stationToStation);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_music_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Snackbar.make(findViewById(android.R.id.content), "No settings defined yet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onTaskCompleted(String result, String call) {

    }
}
