package her.blok2.b_ict.hz.com.blok2her.model;

/**
 * Created by anton on 8-1-16.
 * A Band has one or more Albums
 */
public class Band extends Performer {
    public Band() {
    }

    /**
     *
     * @param name: name of band
     */
    public Band(String name){
        this.setName(name);
    }
    @Override
    public String toString(){
        return getName();
    }
}
