import java.util.ArrayList;

import her.blok2.b_ict.hz.com.blok2her.model.Chart;
import her.blok2.b_ict.hz.com.blok2her.model.MusicItem;

/**
 * Created by svenv on 22-1-2016.
 */
public class listMusicItem {
    /**
     * list all musicItems
     * @return a list of all musicItems
     */
    public static ArrayList<String> listMusicItems(){
        ArrayList<String> returnValue=new ArrayList<String>();
        for (int i=0;i< Chart.musicItems.size();i++){
            MusicItem musicItem= Chart.musicItems.get(i);
            String s=musicItem.toString();
            returnValue.add(s);
        }
        return returnValue;
    }
}
